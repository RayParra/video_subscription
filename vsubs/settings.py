"""
Django settings for vsubs project.

Generated by 'django-admin startproject' using Django 3.2.6.

For more information on this file, see
https://docs.djangoproject.com/en/3.2/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/3.2/ref/settings/
"""

from pathlib import Path
#from ramylu.kdevs.rmdevs.settings import LOGOUT_REDIRECT_URL

# Build paths inside the project like this: BASE_DIR / 'subdir'.
BASE_DIR = Path(__file__).resolve().parent.parent


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/3.2/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'django-insecure-3&yfd=95ioc_iddbg$scue^!7z7d_nynphlf+bitns21*qyqf)'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

BASE_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

LOCAL_APPS = [
    'home',
    'memberships',
]

THIRD_APPS = [
   'rest_framework',
]

INSTALLED_APPS = LOCAL_APPS + BASE_APPS + THIRD_APPS

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'vsubs.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ["templates"],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'vsubs.wsgi.application'


# Database
# https://docs.djangoproject.com/en/3.2/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': BASE_DIR / 'db.sqlite3',
    }
}


# Password validation
# https://docs.djangoproject.com/en/3.2/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/3.2/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/3.2/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = [
    BASE_DIR / "static",
    #'/var/www/static/',
]



# Default primary key field type
# https://docs.djangoproject.com/en/3.2/ref/settings/#default-auto-field

DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'

if DEBUG:
    # local mode
    STRIPE_PUBLISHABLE_KEY = "pk_test_51IO6x8IE0cnZm0fDLon8ZvYo2druAEDoZjbxf8GEUibL4FzvwvXHN2bFBDdfl0wr2dppck5FdorVqnqNuVt0wBDO00ZeBi8j3i"
    STRIPE_SECRET_KEY = "sk_test_51IO6x8IE0cnZm0fDGvgEamCyrB66fOmLF2Sif22qiAiqPg4JjMNzKnv5kyfP1wA8DX5OvedqtLzTihodmoBWnMA500kIhppPOD"
else:
    # deploy mode
    STRIPE_PUBLISHABLE_KEY = "pk_test_51IO6x8IE0cnZm0fDLon8ZvYo2druAEDoZjbxf8GEUibL4FzvwvXHN2bFBDdfl0wr2dppck5FdorVqnqNuVt0wBDO00ZeBi8j3i"
    STRIPE_SECRET_KEY = "sk_test_51IO6x8IE0cnZm0fDGvgEamCyrB66fOmLF2Sif22qiAiqPg4JjMNzKnv5kyfP1wA8DX5OvedqtLzTihodmoBWnMA500kIhppPOD"

LOGIN_REDIRECT_URL = '/'
LOGOUT_REDIRECT_URL = '/'