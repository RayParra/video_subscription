from django.shortcuts import render
from django.views import generic
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.core import serializers
from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status


from .forms import SignupForm
from .models import Client
from .serializers import ClientSerializer
# Create your views here.


class About(generic.TemplateView):
    template_name = "home/about.html"


class Signup(generic.CreateView):
    template_name = "home/signup.html"
    model = User
    form_class = SignupForm
    success_url = reverse_lazy("home:login")



def WSexample(request):
    data = serializers.serialize("json", Client.objects.all())
    return HttpResponse(data, content_type="application/json")


##### C R U D #####

## RETRIEVE or READ ##
# LIST(GET) # CREATE(POST)
@api_view(["GET", "POST"])
def wsClientAPI(request):
    if request.method=="GET":
        data = ClientSerializer(Client.objects.all(), many=True)
        return Response(data=data.data, status=status.HTTP_200_OK)
    elif request.method=="POST":
        data = ClientSerializer(data=request.data)
        if data.is_valid():
            data.save()
            return Response(data.data, status=status.HTTP_201_CREATED)
        return Response(data.errors, status=status.HTTP_400_BAD_REQUEST)