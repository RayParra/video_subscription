from django.urls import path
from django.contrib.auth import views as auth_views

from home import views


app_name = "home"


urlpatterns = [
    path('about/', views.About.as_view(), name="about"),
    path('', auth_views.LoginView.as_view(template_name="home/index.html"), name="login"),
    path('logout/', auth_views.LogoutView.as_view(template_name="home/index.html"), name="logout"),
    path('signup/', views.Signup.as_view(), name="signup"),
    path('wsexample/', views.WSexample, name="wsexample"),
    path('wsClients/', views.wsClientAPI, name="wsclients"),
]
