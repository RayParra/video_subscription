from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.conf import settings
import stripe
# Create your models here.


"""
Memberships
- slug
- membership_type(Free, Professional, Enterprise)
- price
- stripe_plan_id
"""

stripe.api_key = settings.STRIPE_SECRET_KEY


MEMBERSHIP_CHOICES = (
    ("Free", "Free"),
    ("Professional", "Pro"),
)


class Membership(models.Model):
    slug = models.SlugField()
    membership_type = models.CharField(max_length=32, choices=MEMBERSHIP_CHOICES)
    price = models.IntegerField()
    stripe_plan_id = models.CharField(max_length=32)
    
    
    def __str__(self):
        return self.membership_type
    

"""
UserMemberships
- user(ForeignKey to Users)
- stripe_costumer_id
- membership_type(ForeignKey to Memberships)
"""

class UserMembership(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    stripe_costumer_id = models.CharField(max_length=32)
    membership_type = models.ForeignKey(Membership, on_delete=models.CASCADE, null=True)
    
    
    def __str__(self):
        return self.user.username


def post_save_usermembership_create(sender, instance, created, *args, **kwargs):
    if created:
        UserMembership.objects.get_or_create(user=instance)
    user_membership, created = UserMembership.objects.get_or_create(user=instance)
    
    if user_membership.stripe_costumer_id is None or user_membership.stripe_costumer_id=="":
        new_costumer_id = stripe.Customer.create( description=instance.username, email=instance.email)
        user_membership.stripe_costumer_id = new_costumer_id["id"]
        user_membership.save()

post_save.connect(post_save_usermembership_create, sender=settings.AUTH_USER_MODEL)

"""
Subscriptions
- user_membership(ForeignKey to UserMemberships)
- stripe_subscription_id
- active 
"""

class Subscription(models.Model):
    user_membership = models.ForeignKey(UserMembership, on_delete=models.CASCADE)
    stripe_subscription_id = models.CharField(max_length=32, blank=True, null=True)
    active = models.BooleanField(default=True)
    
    def __str__(self) -> str:
        return self.user_membership.user.username