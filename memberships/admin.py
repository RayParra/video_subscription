from django.contrib import admin

# Register your models here.
from memberships.models import Membership, UserMembership, Subscription

@admin.register(Membership)
class MembershipAdmin(admin.ModelAdmin):
    list_display = ["slug", "membership_type", "price", "stripe_plan_id"]


@admin.register(UserMembership)
class UserMembershipAdmin(admin.ModelAdmin):
    list_display = ["user", "membership_type", "stripe_costumer_id"]
    

@admin.register(Subscription)
class SubscriptionAdmin(admin.ModelAdmin):
    list_display = ["user_membership", "active"]